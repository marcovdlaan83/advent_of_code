﻿using System;
using System.Threading;

namespace _2
{
    class Program
    {
        public static readonly int[] input = {1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,19,5,23,1,23,9,27,2,27,6,31,1,31,6,35,2,35,9,39,1,6,39,43,2,10,43,47,1,47,9,51,1,51,6,55,1,55,6,59,2,59,10,63,1,6,63,67,2,6,67,71,1,71,5,75,2,13,75,79,1,10,79,83,1,5,83,87,2,87,10,91,1,5,91,95,2,95,6,99,1,99,6,103,2,103,6,107,2,107,9,111,1,111,5,115,1,115,6,119,2,6,119,123,1,5,123,127,1,127,13,131,1,2,131,135,1,135,10,0,99,2,14,0,0};
        static void Main(string[] args)
        {
            //int[] input = {1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,19,5,23,1,23,9,27,2,27,6,31,1,31,6,35,2,35,9,39,1,6,39,43,2,10,43,47,1,47,9,51,1,51,6,55,1,55,6,59,2,59,10,63,1,6,63,67,2,6,67,71,1,71,5,75,2,13,75,79,1,10,79,83,1,5,83,87,2,87,10,91,1,5,91,95,2,95,6,99,1,99,6,103,2,103,6,107,2,107,9,111,1,111,5,115,1,115,6,119,2,6,119,123,1,5,123,127,1,127,13,131,1,2,131,135,1,135,10,0,99,2,14,0,0};
            //int[] output = Program.RunA(input);
            Program.RunB(input);
            //Console.WriteLine("[{0}]", string.Join(", ", output));
        }

        static int[] RunA(int[] input){
            intCode computer = new intCode(input, 12, 2);
            return computer.Start();
        }

        static void RunB(int[] input){
            for(int counterA = 0; counterA < 100; counterA++){
                for(int counterB = 0; counterB < 100; counterB++){
                    int[] clone = (int[])input.Clone();
                    Console.WriteLine(String.Format("Creating computer {0}, {1}", counterA, counterB));
                    intCode c = new intCode(clone, counterA, counterB);
                    ThreadPool.QueueUserWorkItem(BackgroundTaskWithObject, c);
                }
            }  
            
        }

        static void BackgroundTaskWithObject(Object stateInfo){
            intCode c = (intCode)stateInfo;
            int[] output = c.Start();
            if(output[0] == 19690720)
                Console.WriteLine(String.Format("Found answer in computer {0}, {1}", c.Noun, c.Verb));
        }


    }
}
