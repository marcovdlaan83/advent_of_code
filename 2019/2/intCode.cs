using System;

namespace _2
{
    public class intCode
    {
        private int[] Input;
        public int[] Output {get;set;}
        public int Noun {get;set;}
        public int Verb {get;set;}
        public intCode(int[] input, int noun, int verb){
            Input = input;
            Noun = noun;
            Verb = verb;
            Input[1] = noun;
            Input[2] = verb;
        }

        public int[] Start(){
            int position = 0;
            while(Input[position] != 99){            
                int result = Calculate(Input[position], Input[Input[position+1]], Input[Input[position+2]]);
                Input[Input[position+3]] = result;
                //Console.WriteLine(String.Format("Storing {0} in Position {1}", result, Input[Input[position+2]]));
                position = position + 4;
            }
            Output = Input;
            return Output;

        }
        public static int Calculate(int opcode, int A, int B){
            switch(opcode){
                case 1:
                    return A+B;
                case 2:
                    return A*B;
            }
            throw new ArgumentException("Not a valid opcode");
        }
    }
}