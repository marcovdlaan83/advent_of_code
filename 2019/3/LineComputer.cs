using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

namespace _3
{
    public class LineComputer
    {

        private string[] _Commands;
        private List<Point> _Points;
        public List<Point> Points{
            get {return _Points;}
        }

        public LineComputer(string input){
            _Commands = input.Split(",");
            _Points = new List<Point>();
            _Points.Add(new Point(0,0));
            
        }

        public void Run(){
            foreach(string command in _Commands){
                Execute(command, _Points);
            }
        }

        private void Execute(string command, List<Point> points)
        {
            char cmd = command[0];
            int iterations = int.Parse(command.Substring(1));

            Point startingPoint = points[points.Count -1];

            for(int i=1;i<iterations+1;i++){
                Point p = new Point();
                switch(cmd){
                    case 'U': // up
                        p = new Point(startingPoint.X, startingPoint.Y+i);
                        break;
                    case 'D': // down
                        p = new Point(startingPoint.X, startingPoint.Y-i);
                        break;
                    case 'L': // left
                        p = new Point(startingPoint.X-i, startingPoint.Y);
                        break;
                    case 'R': // right
                        p = new Point(startingPoint.X+i, startingPoint.Y);
                        break;
                }
                points.Add(p);
            }                          
        }

        public static int CalculateManhattanDistance(LineComputer A, LineComputer B)
        {
            IEnumerable<Point> dups = GetCrossings(A, B);
            List<int> distances = new List<int>();
            foreach (Point p in dups)
            {
                distances.Add(System.Math.Abs(p.X) + System.Math.Abs(p.Y));
            }
            return distances.Min();
        }

        public static IEnumerable<Point> GetCrossings(LineComputer A, LineComputer B)
        {
            List<Point> pointsA = RemoveFirstItem(A);
            List<Point> pointsB = RemoveFirstItem(B);
            var dups = pointsA.Intersect(pointsB);
            return dups;
        }

        private static List<Point> RemoveFirstItem(LineComputer A)
        {
            return A.Points.Where(e => e.X != 0 && e.Y != 0).ToList();
        }

        public static int CalculateLeastDistanceCrossing(LineComputer A, LineComputer B){
            var dups = GetCrossings(A, B);
            List<int> distances = new List<int>();
            foreach(var crossing in dups){
                distances.Add(GetDistancePerCrossing(crossing, A, B));
            }
            return distances.Min();
        }

        private static int GetDistancePerCrossing(Point target, LineComputer a, LineComputer b)
        {
            List<Point> pointsA = a.Points;
            List<Point> pointsB = b.Points;

            int idxA = pointsA.FindIndex(p => p.X == target.X && p.Y == target.Y);
            int idxB = pointsB.FindIndex(p => p.X == target.X && p.Y == target.Y);

            return idxA + idxB;
            
        }
    }
}